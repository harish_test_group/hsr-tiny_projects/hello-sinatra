require 'sinatra' 

set :bind, '0.0.0.0'

get '/' do 
  "Welcome to a Kubernetes Demo"
end

get '/hello' do
  "Hello, Support Team!"
end

get '/goodbye' do
  "Goodbye, Support Team!"
end
